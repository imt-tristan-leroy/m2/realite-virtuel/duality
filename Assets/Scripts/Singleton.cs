using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour
{

    public GameObject playerPrefab;
    public GameObject spawn;

    private System.DateTime startTime;

    private void Awake()
    {
        startTime = System.DateTime.UtcNow;
        Instantiate(playerPrefab, spawn.transform.position, spawn.transform.rotation);
    }
}
